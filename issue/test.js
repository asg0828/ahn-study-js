const issue1 = () => {
/**
 * 1번의 경우에는 마지막 요소에만 append가 적용되고
 * 2번은 정상작동 한다.
 */

// 1.
    // tr 생성
    for(i = 0; i < trList.length; i++) {
        trList[i] = document.createElement('tr');
    }

    // td 생성
    for(i = 0; i < tdList.length; i++) {
        tdList[i] = document.createElement('td');
        tdList[i].innerText = 'test' + i;
    }

    // td -> tr -> container
    for(i = 0; i < trList.length; i++) {
        for(j = 0; j < tdList.length; j++) {
            //trList[i].innerHTML += tdList[j].outerHTML;
            trList[i].appendChild(tdList[j]);
        }
        container.append(trList[i]);
    }

// 2.
    // td -> tr -> container 한번에
    for(i = 0; i < trList.length; i++) {
        trList[i] = document.createElement('tr');
        for(j = 0; j < tdList.length; j++) {
            tdList[j] = document.createElement('td');
            tdList[j].innerText = 'test' + j;
            trList[i].appendChild(tdList[j]);
        }
        container.appendChild(trList[i]);
    }
}