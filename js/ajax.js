//common function
const common = () => {

}

//리스트 데이터 추가
const addList = () => {
    let container = document.querySelector('#container');
    if(container.firstChild) return;
    
    let trList = new Array(10);
    let tdList = new Array(10);

    /**
     * 2021.09.05 수정
     * trList, tdList를 따로 처리하지 않고 한번에 처리
     */
    for(i = 0; i < trList.length; i++) {
        trList[i] = document.createElement('tr');
        for(j = 0; j < tdList.length; j++) {
            tdList[j] = document.createElement('td');
            tdList[j].innerText = `test${j}`;
            trList[i].appendChild(tdList[j]);
        }
        container.appendChild(trList[i]);
    }
}

//대상 하위 요소 모두 제거
const deleteList = (elemntId) => {
    let parent = document.getElementById(elemntId);
    /**
     * 2021.09.05 수정
     * hasChildNodes() = > firstChild
     */
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

//리스트 출력용 샘플데이터 생성
const createSampleData = () => {
    let data = {
        name: 'ahn',
        age: 32,ㅇ
    }

    let arr = new Array(10);

    for(i = 0; i < arr.length; i++) {
        data.name  = `ahn${i}`;
        arr[i] = data;
    }
}

//조회
const onSearch = () => {
    const order = document.getElementById('order').value;
    const searchKey = document.getElementById('searchKey').value;
    const keyword = document.getElementById('keyword').value;

    console.log(order, searchKey, keyword);
}

//검색조건 초기화
const clearFilter = () => {
    document.getElementById('order').value = '';
    document.getElementById('searchKey').value = '';
    document.getElementById('keyword').value = '';

    /**
     * 2021.09.05 수정
     * 참조값은 변수에 메모리 주소가 복사되기 때문에 같은 공간을 공유하지만,
     * 기본값은 변수에 값 자체가 복사되기 때문에 다른 메모리 공간이다.
     */
    /*
    let order = document.getElementById('order').value;
    let searchKey = document.getElementById('searchKey').value;
    let keyword = document.getElementById('keyword').value;
    order = '';
    searchKey = '';
    keyword = '';
    */
}

//AJAX
const ajax = () => {
    //http request 객체 생성
    let xhr = new XMLHttpRequest();

    //http request 객체 초기화. method 종류, url, 비동기 여부
    //GET
    xhr.open("GET", "data/test.json", true);
    xhttp.send();

    //POST
    const data = {};
    xhr.open("POST", "data/test.json", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify(data));

    //http request 객체 사용
    xhr.send();

    //http response
    xhr.onload = () => {
        if (xhr.status === 200) {
            xhr.response;
        }
    }
}

//JSON
const json = () => {
    //데이터를 JSON문자열로 변환
    JSON.stringify();

    //JSON문자열을 원본데이터로 변환
    JSON.parse();
}